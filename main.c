#include "stm32f3xx.h"                  // Device header
#include "CommunicationGPS.h" 
#include <stdbool.h> 
#include "Driver_Common.h"

uint8_t data;
uint8_t needUpdate;

#define STOP_MOTOR  0
#define RUN_MOTOR  1
#define INIT_UART 3
#define IMITATION_AZIMUT_O_FOR_MRLS_ENABLE  4				// User send Bluetochcommand == 4 with MobilePhone, and DownPart received that command and born AZIMUT_0_SIGNAL for mrls_2d
																										// That feath is used for readRadData without rotatemrls_2d
#define IMITATION_AZIMUT_O_FOR_MRLS_DISABLE 5
#define RECEIVED_COMM_SPEED_MOTOR_1_SEC   6
#define RECEIVED_COMM_SPEED_MOTOR_1_5_SEC 7
#define RECEIVED_COMM_SPEED_MOTOR_2_SEC   8

 uint16_t angle = 0;
 int grayCode = 0;
 uint32_t is_motor_running = 0;
int chisloTochek = 72;
static bool generateImitationAzimuth0 = false;
uint64_t g_time = 0;
void Born_Azimuth_0_Signal();

uint32_t g_coef_SpeedMotor = 60;
uint32_t g_accelerateSpeedMotor = 100;

uint32_t TIM2_PSC = 60-1 + 10*72; //�������� ��� ���������� �������� ������, ����� �� ������� �������� � ������� ����������


typedef enum{
	SPEED_MOTOR_1_25_SEC,
	SPEED_MOTOR_2_5_SEC,
	SPEED_MOTOR_5_SEC
}tSpeedMotor;

tSpeedMotor motorSpeed = SPEED_MOTOR_2_5_SEC;
tSpeedMotor currentSpeedMotor = SPEED_MOTOR_2_5_SEC;
const uint16_t sinus_0deg[72]={128, 139, 150, 161, 171, 182, 191, 201, 210, 218, 225, 232, 
	238, 243, 247, 251, 253, 255, 255, 255, 253, 251, 247, 243, 238, 232, 225, 218, 210,
	201, 191, 182, 171, 161, 150, 139, 128, 117, 106, 95, 85, 74, 65, 55, 46, 38, 31, 24,
	18, 13, 9, 5, 3, 1, 1, 1, 3, 5, 9, 13, 18, 24, 31, 38, 46, 55, 64, 74, 85, 95, 106, 117};
	
const uint16_t sinus_120deg[72]={238, 232, 225, 218, 210, 201, 192, 182, 171, 161, 150, 139, 128,
	117, 106, 95, 85, 74, 65, 55, 46, 38, 31, 24, 18, 13, 9, 5, 3, 1, 1, 1, 3, 5, 9, 13, 18, 24, 
	31, 38, 46, 55, 64, 74, 85, 95, 106, 117, 128, 139, 150, 161, 171, 182, 191, 201, 210, 218, 
225, 232, 238, 243, 247, 251, 253, 255, 255, 255, 253, 251, 247, 243};


const uint16_t sinus_240deg[72]={18, 13, 9, 5, 3, 1, 1, 1, 3, 5, 9, 13, 18, 24, 31, 38, 46, 55, 
	64, 74, 85, 95, 106, 117, 128, 139, 150, 161, 171, 182, 191, 201, 210, 218, 225, 232, 238, 
	243, 247, 251, 253, 255, 255, 255, 253, 251, 247, 243, 238, 232, 225, 218, 210, 201, 191, 
182, 171, 161, 150, 139, 128, 117, 106, 95, 85, 74, 65, 55, 46, 38, 31, 24};

const uint16_t sinus[72]={18, 13, 9, 5, 3, 1, 1, 1, 3, 5, 9, 13, 18, 24, 31, 38, 46, 55, 
	64, 74, 85, 95, 106, 117, 128, 139, 150, 161, 171, 182, 191, 201, 210, 218, 225, 232, 238, 
	243, 247, 251, 253, 255, 255, 255, 253, 251, 247, 243, 238, 232, 225, 218, 210, 201, 191, 
182, 171, 161, 150, 139, 128, 117, 106, 95, 85, 74, 65, 55, 46, 38, 31, 24};





bool neededSwitchSpeedMotor = false;

void saveSpeedMotor( tSpeedMotor speed ){
 
	if ( currentSpeedMotor == speed )
		return;
	else{
		motorSpeed = speed;
		currentSpeedMotor = speed;
		neededSwitchSpeedMotor = true;
	}
}
void setSpeedMotor( tSpeedMotor speed ){

	switch ( speed ){
	
		case SPEED_MOTOR_1_25_SEC    :  g_coef_SpeedMotor = 120; break;
		case SPEED_MOTOR_2_5_SEC  :  g_coef_SpeedMotor = 60; break;
		case SPEED_MOTOR_5_SEC    :  g_coef_SpeedMotor = 43
			
		; break;
		
	}
	
}

void processGenerateImitationAzimuth0(){

			
						if( generateImitationAzimuth0 ){   // if we imitation rotate 
							g_time++;
								if ( g_time > 3850000){
												g_time = 0;
												GPIOA->ODR |= 1<<0;
												for (int i=0;i<10000;i++);
												GPIOA->ODR &= ~(1<<0);
								}
						}
}
void Born_Azimuth_0_Signal(){

//	if ( g_time > 3000000){
//		g_time = 0;

//			GPIOC->ODR |= 1<<5;
////			for(uint32_t i = 0; i < 100000; i++){}
//			GPIOC->ODR |= 0<<5;
//	}

}
void TIM15_IRQHandler (void){
	TIM15->SR&=~1; //Interrupt flag disabled	
	initGPS();
		
		
		
		USART3->CR1|= USART_CR1_RXNEIE; // enable RX interrupt
	
		NVIC_SetPriority (USART3_IRQn,10);
    NVIC_EnableIRQ (USART3_IRQn);
}
void TIM2_IRQHandler (void){
	TIM2->SR&=~1; //Interrupt flag disabled	
	
	
	
	if ((is_motor_running==1)&(TIM2_PSC==g_coef_SpeedMotor-1 + g_accelerateSpeedMotor))//двигатель включился
	{
		TIM2_PSC -=2;
		TIM2->PSC = TIM2_PSC;
		//Configure PC0 as TIM1_CH1
		GPIOC->MODER|=GPIO_MODER_MODER0_1; // Configure PC0 as alternate function
		GPIOC->AFR[0]|=2;//Configure PC0 as TIM1_CH1
	
		//Configure PC1 as TIM1_CH2
		GPIOC->MODER|=GPIO_MODER_MODER1_1; // Configure PC1 as alternate function
		GPIOC->AFR[0]|=2<<4;//Configure PC1 as TIM1_CH1
	
		//Configure PC2 as TIM1_CH3
		GPIOC->MODER|=GPIO_MODER_MODER2_1; // Configure PC2 as alternate function
		GPIOC->AFR[0]|=2<<8;//Configure PC2 as TIM1_CH1
		
		GPIOC->ODR |= 7<<10; //enable motor
	}
	
	if (TIM2_PSC == g_coef_SpeedMotor - 1) //стационарный режим
	{
		TIM2->DIER &= ~TIM_DIER_UIE; //��������� ����������		
	}
	else
		if (TIM2_PSC < g_coef_SpeedMotor-1 + g_accelerateSpeedMotor)  //если ускоряется
	{
		TIM2_PSC -=2;
		TIM2->PSC = TIM2_PSC;		
	}
	
	
	
	
	
}
uint16_t gray2bin(uint16_t n1) {
  uint16_t n2 = n1;
  while (n1 >>= 1) n2 ^= n1;
  return n2;
}
void SPI3_IRQHandler (void){		
	//angle = gray2bin(SPI3->DR)&0x7FFF;
	grayCode = SPI3->DR;

	uint32_t azimuth = (gray2bin(grayCode)&0x7FFF);
	
	if ( !generateImitationAzimuth0 ){
	
	
			 if ( azimuth>100  && azimuth<120  ) //���� 14-� ��� ����� 1
			 {
				 GPIOC->ODR |= 1<<5;
				 GPIOA->ODR |= 1<<7;
				 GPIOA->ODR |= 1<<0;
			 }
			 else 
			 {
				 GPIOC->ODR &= ~(1<<5);
				 GPIOA->ODR &= ~(1<<7);
				 GPIOA->ODR &= ~(1<<0);
			 }	
			 
		 }
//			 if ((grayCode&0x4000)>0) //���� 14-� ��� ����� 1
//			 {
//				 GPIOC->ODR |= 1<<5;
//				 GPIOA->ODR |= 1<<7	;
//			 }
//			 else 
//			 {
//				 GPIOC->ODR &= ~(1<<5);
//				 GPIOA->ODR &= ~(1<<7);
//			 }
		//if (angle>7000) GPIOA->ODR |= 1<<7	;
		//else GPIOA->ODR &= ~(1<<7);
		 	
} 
void USART3_IRQHandler(void){
	uint32_t data = 0;	
	data = USART3->RDR; // ������ �� RDR ���������� ����������
	USART1->TDR = data;
}
void runMotor(){
			
		TIM2->DIER |= TIM_DIER_UIE; //�������� ���������� �� ������� ��� ������ ��������� ������
		TIM2_PSC = g_coef_SpeedMotor - 1 + g_accelerateSpeedMotor;
	  TIM2->PSC = TIM2_PSC;
 	  is_motor_running = 1;
}
void stopMotor(){
		
	is_motor_running = 0;

		GPIOC->MODER&=~GPIO_MODER_MODER0_1;
		GPIOC->MODER&=~GPIO_MODER_MODER1_1;
		GPIOC->MODER&=~GPIO_MODER_MODER2_1;
		
		GPIOC->ODR &= ~(7<<10); //disable motor;
}
void USART2_IRQHandler(void){
	uint32_t command = 0;
	
	command = USART2->RDR; // ������ �� RDR ���������� ����������
	
	
	
	if (command == INIT_UART)
	{
		while ((USART2->ISR & USART_ISR_TXE)==0){}; //���, ����� ���������� ������ ����� ����������
		USART2->TDR = INIT_UART;			
	}
	else if (command == STOP_MOTOR)
	{
		stopMotor();
		generateImitationAzimuth0 = true;	
	}
	else if (command == RUN_MOTOR)
	{
		generateImitationAzimuth0 = false;
		runMotor();	
	}
	else if (command == IMITATION_AZIMUT_O_FOR_MRLS_ENABLE)
	{
		generateImitationAzimuth0 = true;			// ______|-|________________________________________|-|_________	T >= 2,5 sec	for correct work mrls_2D
	}
	else if (command == IMITATION_AZIMUT_O_FOR_MRLS_DISABLE)
	{
		GPIOC->ODR |= 0<<5;
		generateImitationAzimuth0 = false;			// _____________________________________________________________	T >= 2,5 sec	for correct work mrls_2D
	}
	
	
	
	
	else if (command == RECEIVED_COMM_SPEED_MOTOR_1_SEC)
	{
		//generateImitationAzimuth0 = true;
		//stopMotor();
		saveSpeedMotor( SPEED_MOTOR_1_25_SEC );
		//runMotor();
	}
	else if (command == RECEIVED_COMM_SPEED_MOTOR_1_5_SEC)
	{
		//generateImitationAzimuth0 = false;
		//stopMotor();
		saveSpeedMotor( SPEED_MOTOR_2_5_SEC );
		//runMotor();
	}
	else if (command == RECEIVED_COMM_SPEED_MOTOR_2_SEC)
	{
	//	stopMotor();
    saveSpeedMotor( SPEED_MOTOR_5_SEC );
//		runMotor();
	}
	
}
void RCC_Init(){
RCC->CR |= RCC_CR_HSEON; //HSE ENABLE
while (!RCC->CR & RCC_CR_HSERDY); //wait HSE Start

FLASH->ACR |= FLASH_ACR_PRFTBE | FLASH_ACR_LATENCY_1;

RCC->CFGR |= RCC_CFGR_HPRE_DIV2; //6���
RCC->CFGR |= RCC_CFGR_PPRE1_DIV1;
RCC->CFGR |= RCC_CFGR_PPRE2_DIV1;

RCC->CFGR &= ~RCC_CFGR_PLLMUL;
RCC->CFGR |= RCC_CFGR_PLLMUL10; //60 ���
RCC->CFGR &= ~RCC_CFGR_PLLXTPRE;
RCC->CFGR2 &= ~RCC_CFGR2_PREDIV;
RCC->CFGR2 |= RCC_CFGR2_PREDIV_DIV1;//HSE INPUT DIV IN PLL
RCC->CFGR &= ~RCC_CFGR_PLLSRC;
RCC->CFGR |= RCC_CFGR_PLLSRC_HSE_PREDIV;

//VKL MNOZHITELY CHASTOTI
RCC->CR |= RCC_CR_PLLON;//wait PLL Start
while (!(RCC->CR & RCC_CR_PLLRDY));

//SWITCH SYSTEM CLOCK
RCC->CFGR &= ~RCC_CFGR_SW;
RCC->CFGR |= RCC_CFGR_SW_PLL;
while ( (RCC->CFGR & RCC_CFGR_SWS)!= RCC_CFGR_SWS_1 );
//RCC->CFGR |= RCC_CFGR_SW_HSE;
//while ( (RCC->CFGR & RCC_CFGR_SWS)!= RCC_CFGR_SWS_0 );
}
void MOTOR_EN_Init(){
	//Configure PC10 as MOTOR_IN1
	GPIOC->MODER|=GPIO_MODER_MODER10_0; // Configure PC10 as output
	GPIOC->ODR &= ~(1<<10); //switch off motor_in1
	
	//Configure PC11 as MOTOR_IN2
	GPIOC->MODER|=GPIO_MODER_MODER11_0; // Configure PC11 as output
	GPIOC->ODR &= ~(1<<11); //switch off motor_in2
	
	//Configure PC12 as MOTOR_IN3
	GPIOC->MODER|=GPIO_MODER_MODER12_0; // Configure PC12 as output
	GPIOC->ODR &= ~(1<<12); //switch off motor_in3
}
void DMA1_Init(void){
	RCC->AHBENR|=RCC_AHBENR_DMA1EN;//enable clock for DMA1
	
	DMA1_Channel5->CCR=0;
	DMA1_Channel5->CPAR = (int) & TIM1->CCR1;//adress of the receiver
  DMA1_Channel5->CMAR = (int) & sinus_0deg[0];
	DMA1_Channel5->CNDTR = chisloTochek;
	DMA1_Channel5->CCR = 2<<DMA_CCR_PL_Pos| //Very High Channel priority level
											 1<<DMA_CCR_MINC_Pos| //Memory increment mode enabled
											 1<<DMA_CCR_MSIZE_Pos| //16 ���
											 1<<DMA_CCR_PSIZE_Pos| //16 ���
											 1<<DMA_CCR_CIRC_Pos| //Circular mode enabled
											 1<<DMA_CCR_DIR_Pos; // Data transfer direction - Read from memory
	DMA1_Channel5->CCR |= 1; //Enable Channel 5
	
	DMA1_Channel7->CCR=0;
	DMA1_Channel7->CPAR = (int) & TIM1->CCR2;//adress of the receiver
  DMA1_Channel7->CMAR = (int) & sinus_120deg[0];
	DMA1_Channel7->CNDTR = chisloTochek;
	DMA1_Channel7->CCR = 2<<DMA_CCR_PL_Pos| //Very High Channel priority level
											 1<<DMA_CCR_MINC_Pos| //Memory increment mode enabled
											 1<<DMA_CCR_MSIZE_Pos| //16 ���
											 1<<DMA_CCR_PSIZE_Pos| //16 ���
											 1<<DMA_CCR_CIRC_Pos| //Circular mode enabled
											 1<<DMA_CCR_DIR_Pos; // Data transfer direction - Read from memory
	DMA1_Channel7->CCR |= 1; //Enable Channel 7
	
	DMA1_Channel1->CCR=0;
	DMA1_Channel1->CPAR = (int) & TIM1->CCR3;//adress of the receiver
  DMA1_Channel1->CMAR = (int) & sinus_240deg[0];
	DMA1_Channel1->CNDTR = chisloTochek;
	DMA1_Channel1->CCR = 2<<DMA_CCR_PL_Pos| //Very High Channel priority level
											 1<<DMA_CCR_MINC_Pos| //Memory increment mode enabled
											 1<<DMA_CCR_MSIZE_Pos| //16 ���
											 1<<DMA_CCR_PSIZE_Pos| //16 ���
											 1<<DMA_CCR_CIRC_Pos| //Circular mode enabled
											 1<<DMA_CCR_DIR_Pos; // Data transfer direction - Read from memory
	DMA1_Channel1->CCR |= 1; //Enable Channel 1
	
	
	//������ ������ SPI3 �� ������� 16
	DMA1_Channel3->CCR=0;
	DMA1_Channel3->CPAR = (int) & SPI3->DR;//adress of the receiver
  DMA1_Channel3->CMAR = (int) & sinus[0];
	DMA1_Channel3->CNDTR = chisloTochek;
	DMA1_Channel3->CCR = 2<<DMA_CCR_PL_Pos| //Very High Channel priority level
											 1<<DMA_CCR_MINC_Pos| //Memory increment mode enabled
											 1<<DMA_CCR_MSIZE_Pos| //16 ���
											 1<<DMA_CCR_PSIZE_Pos| //16 ���
											 1<<DMA_CCR_CIRC_Pos| //Circular mode enabled
											 1<<DMA_CCR_DIR_Pos; // Data transfer direction - Read from memory
	DMA1_Channel3->CCR |= 1; //Enable Channel 3
}



void init_all(){

	RCC_Init();	
	RCC->AHBENR |= RCC_AHBENR_GPIOAEN|RCC_AHBENR_GPIOBEN|RCC_AHBENR_GPIOCEN|RCC_AHBENR_GPIODEN; //IO port A, B, C, D clock enable
	RCC->APB1ENR |= RCC_APB1ENR_TIM2EN ; //TIM2 clock enable
	RCC->APB2ENR |= RCC_APB2ENR_TIM1EN|RCC_APB2ENR_TIM16EN; //TIM1 and TIM15 clock enable
	RCC->APB1ENR |= RCC_APB1ENR_SPI3EN; //Enable SPI3	
	MOTOR_EN_Init();	
	//Configure PA12 as FAN driver
	GPIOA->MODER|=GPIO_MODER_MODER12_0; // Configure PD12 as output
	//Configure PA7 
	GPIOA->MODER|=GPIO_MODER_MODER7_0; // 
	GPIOA->ODR |= 1<<7; 
	
	
	
//	//Configure PC5 as FPGA Trigger
//	GPIOC->MODER|=GPIO_MODER_MODER5_0; // Configure PC5 as output
//	GPIOC->MODER&=~GPIO_MODER_MODER5_1; // Configure PC5 as output
//	GPIOC->ODR |= 1<<5; //
	
		//Configure PC5 as FPGA Trigger
	GPIOA->MODER|=GPIO_MODER_MODER0_0; // Configure PC5 as output
	GPIOA->MODER&=~GPIO_MODER_MODER0_1; // Configure PC5 as output
	GPIOA->ODR |= 1<<0; //
	
	
	
	//Configure PB2 as USER_LED	
	GPIOB->MODER|=GPIO_MODER_MODER2_0; // Configure PB2 as output
	GPIOB->ODR |= 1<<2; //USER_LED = 1
	//Configure PD2 as ENCODER_DE	
	GPIOD->MODER|=GPIO_MODER_MODER2_0; // Configure PD2 as output
	GPIOD->ODR |= 1<<2; //ENCODER_DE = 1
	//Configure PB5 as ECNODER_nRE	
	GPIOB->MODER|=GPIO_MODER_MODER5_0; // Configure PB5 as output
	GPIOB->ODR &= ~(1<<5); //ENCODER_nRE = 0
	//Configure PB3 as ENCODER_SCK
	GPIOB->AFR[0]= 6<<12;
	GPIOB->MODER &= ~GPIO_MODER_MODER3_0; // Configure PB3 in Alternate function mode
	GPIOB->MODER |= GPIO_MODER_MODER3_1; // Configure PB3 in Alternate function mode
	GPIOB->PUPDR  |= GPIO_PUPDR_PUPDR3_1; //Pull-dowd - ��� ����, ����� ��� ������������ ������� PB3 � ���������� �� ������� 15 ��� 0
	//Configure PB4 as ENCODER_MISO
	GPIOB->AFR[0]|= 6<<16;	
	GPIOB->MODER &= ~GPIO_MODER_MODER4_0; // Configure PB3 in Alternate function mode
	GPIOB->MODER |= GPIO_MODER_MODER4_1; // Configure PB3 in Alternate function mode
	//Configure PA7 as input with pull-up resistor (MoTOR on/OFF)
	GPIOA->PUPDR|=GPIO_PUPDR_PUPDR7_0; // Configure PA7 pull-up

	SPI3->CR1 = 
	0<<SPI_CR1_LSBFIRST_Pos     //MSB first    
	| 0x07<<SPI_CR1_BR_Pos        //�������� ��������: F_PCLK/256 (~235���)
	| 1<<SPI_CR1_MSTR_Pos         //����� Master (�������)
	| 0<<SPI_CR1_CPOL_Pos   //CK to 0 when idle
	| 0<<SPI_CR1_CPHA_Pos; //The second clock transition is the first data capture edge

	SPI3->CR2|=1<<2; //SS output enable (��� ����� �� ��������
	SPI3->CR2 |= 0x0F00;
	SPI3->CR2 |= SPI_CR2_RXNEIE;	
	SPI3->CR1 |= 1<<SPI_CR1_SPE_Pos; //�������� SPI
	NVIC_SetPriority (SPI3_IRQn,3);
	NVIC_EnableIRQ(SPI3_IRQn);
	TIM16->PSC = 6000 - 1;
	TIM16->ARR = 1; // 0.1 ��	
	TIM16->DIER |= 1<<TIM_DIER_UDE_Pos; // DMA enabled
	TIM16->CR1|=1;//counter enabled;
	//NVIC_SetPriority(TIM16_IRQn,11);
	//NVIC_EnableIRQ(TIM16_IRQn);
	RCC->AHBENR|=RCC_AHBENR_GPIOAEN|RCC_AHBENR_GPIODEN|RCC_AHBENR_GPIOCEN; //IO port A, IO port D, IO port C clock enable
	RCC->APB1ENR|=RCC_APB1ENR_TIM2EN ; //TIM2 clock enable
	RCC->APB2ENR|=RCC_APB2ENR_TIM1EN|RCC_APB2ENR_TIM15EN|RCC_APB2ENR_TIM16EN; //TIM1, TIM15 and TIM16 clock enable
	//Configure PD12 as output to drive LED
	//GPIOD->MODER|=GPIO_MODER_MODER2_0; // Configure PD12 as output
	//GPIOD->ODR |= 1<<2; //Switch on the LE	
	GPIOC->ODR &= ~(7<<10); //Switch off the EN1, EN2, EN3
	TIM1->PSC = 2-1;
	TIM1->ARR = 256;
	//����. ����������
	TIM1->CCR1 = sinus_0deg[0];
	TIM1->CCR2 = sinus_120deg[0];
	TIM1->CCR3 = sinus_240deg[0];
	//�������� �� ����� ����� 1, �������� ������� ������
	TIM1->CCER |= TIM_CCER_CC1E | TIM_CCER_CC1P;
	//�������� �� ����� ����� 2, �������� ������� ������ 
	TIM1->CCER |= TIM_CCER_CC2E | TIM_CCER_CC2P;
	//�������� �� ����� ����� 3, �������� ������� ������
	TIM1->CCER |= TIM_CCER_CC3E | TIM_CCER_CC3P;
	//�������� ������������ ������ ������� ��� ������
	TIM1->BDTR |= TIM_BDTR_MOE;
	//PWM mode 1, ������ ��� 1 �����
	TIM1->CCMR1 = TIM_CCMR1_OC1M_2 | TIM_CCMR1_OC1M_1;
	//PWM mode 1, ������ ��� 1 �����
	TIM1->CCMR1 |= TIM_CCMR1_OC2M_2 | TIM_CCMR1_OC2M_1;
	//PWM mode 1, ������ ��� 1 �����
	TIM1->CCMR2 = TIM_CCMR2_OC3M_2 | TIM_CCMR2_OC3M_1;
	//������� �����
	TIM1->CR1 &= ~TIM_CR1_DIR;
	//������������ �� ������, Fast PWM
	TIM1->CR1 &= ~TIM_CR1_CMS;
	//�������� �������
	TIM1->CR1 |= TIM_CR1_CEN;	
	TIM2->PSC = TIM1->PSC;
	TIM2->ARR = 10*256;
	TIM2->CCR1 = TIM2->ARR-10;
	TIM2->CCR2 = TIM2->ARR-10;
	TIM2->CCR3 = TIM2->ARR-10;
	TIM2->DIER |= TIM_DIER_CC1DE|TIM_DIER_CC2DE|TIM_DIER_CC3DE; // interrupt enable
	NVIC_SetPriority(TIM2_IRQn,15);
	NVIC_EnableIRQ(TIM2_IRQn);
	DMA1_Init();
	TIM2->CR1|=1;//counter enabled;
	
	////////////////////////////////////////////////////////////////////////////////
	RCC->APB1ENR|=RCC_APB1ENR_USART2EN; //USART2 clock enable
	RCC->AHBENR|=RCC_AHBENR_GPIOAEN; //IO port A clock enable	
	GPIOA->AFR[0]|= (7<<8)|(7<<12); //Alternate function 7
	GPIOA->MODER|=GPIO_MODER_MODER2_1|GPIO_MODER_MODER3_1;//PA2 and PA3 in Alternate function mode
	USART2 -> CR1 &= ~(USART_CR1_OVER8);
	USART2 -> BRR = 60000000  / 9600;
	//USART1 -> CR1 |= USART_CR1_TCIE;
	USART2 -> CR1 |= USART_CR1_RE | USART_CR1_TE;
	USART2 -> CR1 |= USART_CR1_UE;
	USART2->CR1|= USART_CR1_RXNEIE; // enable RX interrupt
	NVIC_SetPriority (USART2_IRQn,2);
	NVIC_EnableIRQ (USART2_IRQn);
///////////////////////////////////////////////////////////////////////////////	
	RCC->APB2ENR|=RCC_APB2ENR_USART1EN; //USART1 clock enable	
	GPIOC->AFR[0]|= 7<<16; //Alternate function 7
	GPIOC->MODER|=GPIO_MODER_MODER4_1;//PC4 in Alternate function mode
	USART1 -> CR1 &= ~(USART_CR1_OVER8);
	USART1 -> BRR = 60000000  / 115200;
	//USART1 -> CR1 |= USART_CR1_TCIE;
	USART1 -> CR1 |= USART_CR1_TE;
	USART1 -> CR1 |= USART_CR1_UE;
	//NVIC_SetPriority (USART1_IRQn,5);
	//NVIC_EnableIRQ (USART1_IRQn);	
	RCC->APB1ENR|=RCC_APB1ENR_USART3EN; //USART3 clock enable
	RCC->AHBENR|=RCC_AHBENR_GPIOBEN; //IO port B clock enable
	GPIOB->AFR[1]|= (7<<8)|(7<<12); //Alternate function 7
	GPIOB->MODER|=GPIO_MODER_MODER10_1|GPIO_MODER_MODER11_1;//PB10 and PB11 in Alternate function mode
	USART3 -> CR1 &= ~(USART_CR1_OVER8);
	USART3 -> BRR = 60000000  / 9600;
	//USART1 -> CR1 |= USART_CR1_TCIE;
	USART3 -> CR1 |= USART_CR1_TE;
	USART3 -> CR1 |= USART_CR1_UE;


	/*TIM15->PSC = 60000;
	TIM15->ARR = 100;	
	TIM15->DIER |=TIM_DIER_UIE;

	NVIC_SetPriority(TIM15_IRQn,14);
	NVIC_EnableIRQ(TIM15_IRQn);
	TIM15->CR1|=1;//counter enabled;*/

	initGPS();
	USART3 -> CR1 |= USART_CR1_RE;	
	USART3->CR1|= USART_CR1_RXNEIE; // enable RX interrupt
	NVIC_SetPriority (USART3_IRQn,10);
	NVIC_EnableIRQ (USART3_IRQn);

	GPIOA->ODR |= 1<<12; //switch FAN on	
}



int i = 0;
	

void processSwitchSpeedMotor(){

	if ( neededSwitchSpeedMotor ){
		
		
		generateImitationAzimuth0 = false;    // выключаем генерацию азимутов0
		stopMotor();	
			for(int i = 0 ; i < 30000000; i++){}
			setSpeedMotor( motorSpeed );
			//for(i = 0 ; i < 30000000; i++){}
		runMotor();	
		neededSwitchSpeedMotor = false;
	}
}
int main (void)
{	
	
	init_all();
	
//	for(int i = 0 ; i < 50000000; i++){}
//			runMotor();	
	
	 
generateImitationAzimuth0 = true;		// 
	
	while(1)
	{	
			processGenerateImitationAzimuth0();
			processSwitchSpeedMotor();
	}
}
/*
 uint16_t SPI3_Read(void)
{ 
	SPI3->DR = 0xFF;//��������� �����
			while(!(SPI3->SR & 1)); //while Rx buffer empty
  //���������� �������� ������ ���������	
  return SPI3->DR;
}

 void TIM16_IRQHandler (void)
{	
	TIM16->SR&=~1;
		
	angle = SPI3_Read();
		 if (is_motor_running)
		 {
			 while ((USART2->ISR & USART_ISR_TXE)==0){};
				USART2->TDR = (angle>>7)&(~1);
			while ((USART2->ISR & USART_ISR_TXE)==0){};
				USART2->TDR = (angle>>1)|1;
		
		 }	
}*/
